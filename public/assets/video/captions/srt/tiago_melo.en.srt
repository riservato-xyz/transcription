1
00:00:00,664 --> 00:00:03,178
this violent past is evident here

2
00:00:03,408 --> 00:00:05,802
in the extraordinary 
Hutton in the Forest

3
00:00:07,392 --> 00:00:11,041
built from 1350 
over a period of 500 years...

4
00:00:11,412 --> 00:00:14,315
the great house has been in 
the hands of only two families:

5
00:00:14,792 --> 00:00:17,762
the De Hotons 
and the Fletcher-Vanes...

6
00:00:19,531 --> 00:00:20,553
legend has it:

7
00:00:20,684 --> 00:00:23,535
that this was the setting 
for the story of king Arthur...

8
00:00:23,619 --> 00:00:24,673
and the Green Knight...

9
00:00:25,332 --> 00:00:27,500
The Green Knight's Castle!

10
00:00:27,949 --> 00:00:28,716
Once...

11
00:00:28,716 --> 00:00:31,309
this stately 
home was a fortress!

12
00:00:31,810 --> 00:00:34,422
As can be seen,
from this pele tower:

13
00:00:34,644 --> 00:00:36,699
still part of the great house!

14
00:00:37,671 --> 00:00:38,708
The pele tower...

15
00:00:38,931 --> 00:00:41,956
was a common architectural 
feature in the north of England:

16
00:00:42,392 --> 00:00:44,672
to protect the against 
cross border raids.

17
00:00:45,310 --> 00:00:48,578
The conflict between 
Scotland and England continued

18
00:00:48,741 --> 00:00:51,647
until the Act of Union in 1707.

19
00:00:53,271 --> 00:00:55,515
so... let's get my...
peach chutney started!

20
00:00:55,627 --> 00:00:58,392
I'm cutting... 
peaches, shallots and pancetta!

21
00:00:58,465 --> 00:01:00,502
with really good,
uniform knife cuts

22
00:01:02,179 --> 00:01:04,338
now, I want you guys 
to add a little booz...

23
00:01:04,342 --> 00:01:05,367
<i>*gasps*

24
00:01:05,367 --> 00:01:06,406
bourbon!

25
00:01:06,508 --> 00:01:07,616
so, those around...

26
00:01:07,694 --> 00:01:08,721
now?

27
00:01:08,715 --> 00:01:09,624
asparagus!

28
00:01:09,595 --> 00:01:10,461
asparagus!

29
00:01:10,461 --> 00:01:11,503
break your asparagus...

30
00:01:12,299 --> 00:01:14,669
alright! we're heating up our
pan to, like... medium-ish?

31
00:01:15,772 --> 00:01:17,414
alright! guess what I 
am ready to do, now?

32
00:01:17,712 --> 00:01:18,741
plate it!

33
00:01:19,313 --> 00:01:20,317
ok, guys.. it's ok y'all!

34
00:01:20,317 --> 00:01:21,490
just gonna strip this down...

35
00:01:21,490 --> 00:01:23,321
I'm gonna put it into... 
uh... bowl here!

36
00:01:23,873 --> 00:01:24,890
alright! so: oil...

37
00:01:24,890 --> 00:01:27,393
I'mma put a salt,
I'm gonna scatter this out...

38
00:01:27,393 --> 00:01:28,872
evenly! across the sheet pan...

39
00:01:29,143 --> 00:01:30,434
into the oven: boom!

40
00:01:30,761 --> 00:01:32,179
so, we're gonna take our corn...

41
00:01:32,330 --> 00:01:34,695
add half of our corn...
in here! ok?!

42
00:01:34,695 --> 00:01:36,816
and the other half: I'mma 
put into this high speed blender

43
00:01:40,223 --> 00:01:41,589
you want your corn puree...

44
00:01:41,589 --> 00:01:42,610
you wanna full that in

45
00:01:42,772 --> 00:01:44,027
I'm gonna finish this
with some chive

46
00:01:44,027 --> 00:01:45,109
did you see that?

47
00:01:45,253 --> 00:01:46,430
all this beautiful residue...

48
00:01:46,932 --> 00:01:48,605
I'm gonna make a <s> sauce
out of this. ok?!

49
00:01:48,724 --> 00:01:49,756
we add some honey to this...

50
00:01:49,892 --> 00:01:50,903
a cup of chicken stock...

51
00:01:51,691 --> 00:01:53,110
and we let this
reduce down to a syrup.

52
00:01:55,667 --> 00:01:57,483
see... let's see, everybody!

53
00:01:58,863 --> 00:02:01,541
um... minimize myself?
I don't need to see myself!

54
00:02:01,541 --> 00:02:02,873
I only need to see you guys!

55
00:02:03,674 --> 00:02:05,211
I need to see you guys!

56
00:02:07,375 --> 00:02:09,117
Hello world! It's Raj!

57
00:02:09,352 --> 00:02:11,856
I am high for this live session!

58
00:02:12,507 --> 00:02:13,349
yo!

59
00:02:13,813 --> 00:02:15,846
class is in session, everybody!

60
00:02:16,254 --> 00:02:17,707
we're about to do some math...

61
00:02:17,951 --> 00:02:18,844
this live session!

62
00:02:18,838 --> 00:02:19,991
so, I'm super excited!

63
00:02:20,186 --> 00:02:22,484
but, first of all:
let me take some roll call.

64
00:02:22,589 --> 00:02:24,288
alright?! so, let's see...

65
00:02:24,288 --> 00:02:24,804
Collin...

66
00:02:24,804 --> 00:02:25,344
Brandon...

67
00:02:25,344 --> 00:02:25,794
Neil...

68
00:02:25,833 --> 00:02:26,304
David...

69
00:02:26,304 --> 00:02:27,073
Prakash...

70
00:02:27,325 --> 00:02:28,003
Sebastian...

71
00:02:28,036 --> 00:02:28,704
Raj...

72
00:02:28,867 --> 00:02:29,632
Spencer...

73
00:02:29,860 --> 00:02:30,629
Naresh...

74
00:02:31,631 --> 00:02:32,324
Nickel...

75
00:02:32,329 --> 00:02:33,029
Clement...

76
00:02:34,655 --> 00:02:35,539
hi, guys?!

77
00:02:35,645 --> 00:02:36,209
Michael...

78
00:02:36,282 --> 00:02:36,874
Benjamin...

79
00:02:37,696 --> 00:02:38,347
Alright!

80
00:02:38,571 --> 00:02:39,760
so... now, that was roll call!

81
00:02:40,235 --> 00:02:43,012
um...
welcome to this live session!

82
00:02:43,051 --> 00:02:44,267
um... for dee..

83
00:02:44,275 --> 00:02:44,834
for dee...

84
00:02:44,935 --> 00:02:47,382
deep learning...
intro to deep learning course!

85
00:02:47,633 --> 00:02:50,422
ok! 
this going to be so... awesome!

86
00:02:50,422 --> 00:02:52,701
because I have been waiting
do to some math

87
00:02:52,874 --> 00:02:54,028
and guess what, guys?!
guess what?!

88
00:02:54,123 --> 00:02:57,214
I bought... this.. um... pad!

89
00:02:57,365 --> 00:02:58,818
to write some math on.

90
00:02:58,818 --> 00:03:00,743
ok! so I've never used 
this before in livestream

91
00:03:00,753 --> 00:03:02,146
so I'm super excited for this

92
00:03:02,543 --> 00:03:04,963
I'm gonna show you 
guys the math behind...

93
00:03:05,007 --> 00:03:06,232
linear... regression.

94
00:03:06,232 --> 00:03:07,618
by the end of this video...

95
00:03:07,897 --> 00:03:09,780
you, guys! are going to...

96
00:03:10,439 --> 00:03:12,233
are goin to know...

97
00:03:12,317 --> 00:03:13,681
like the back of your hand...

98
00:03:13,776 --> 00:03:15,118
how to do radial regression!

99
00:03:15,353 --> 00:03:17,409
that includes... 
gradient descent!

100
00:03:17,565 --> 00:03:19,347
and guess what?
we use gradient descent

101
00:03:19,571 --> 00:03:21,234
all over the place 
in machine learning

102
00:03:21,390 --> 00:03:22,930
don't worry if  you don't 
know what that is...

103
00:03:22,935 --> 00:03:24,399
I'm gonna show it to you. ok?!

104
00:03:24,539 --> 00:03:26,359
so we're gonna 
deep dive into this!

105
00:03:26,605 --> 00:03:27,150
so...

106
00:03:27,262 --> 00:03:27,858
um...
