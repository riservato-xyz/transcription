1
00:00:00,664 --> 00:00:03,178
Esse passado violento 
fica evidente, aqui...

2
00:00:03,408 --> 00:00:05,802
na extraordinária 
casa de <i>Hutton in the Forest</i>.

3
00:00:07,392 --> 00:00:11,041
Iniciadas as construções em 1350
e terminadas 500 anos depois

4
00:00:11,412 --> 00:00:14,315
A grande casa pertenceu à 
duas famílias, apenas: 

5
00:00:14,792 --> 00:00:17,762
aos <i>De Hotons </i>
e aos <i>Fletcher-Vanes</i>.

6
00:00:19,531 --> 00:00:20,553
A lenda também diz:

7
00:00:20,684 --> 00:00:23,535
a casa serviu de cenário 
para a história do Rei Artur...

8
00:00:23,619 --> 00:00:24,673
e também do Cavaleiro Verde.

9
00:00:25,332 --> 00:00:27,500
O Castelo do Cavaleiro Verde!

10
00:00:27,949 --> 00:00:28,716
Antes...

11
00:00:28,716 --> 00:00:31,309
este majestoso local 
fora uma fortaleza!

12
00:00:31,810 --> 00:00:34,422
Como vemos, 
pela torre de cascalhos:

13
00:00:34,644 --> 00:00:36,699
ainda faz jus à Grande Casa!

14
00:00:37,671 --> 00:00:38,708
Torres de cascalhos...

15
00:00:38,931 --> 00:00:41,956
eram muito comum na 
arquitetura do Norte Inglês:

16
00:00:42,392 --> 00:00:44,672
serviam como proteção 
à invasões de fronteiras.

17
00:00:45,310 --> 00:00:48,578
O conflito entre 
Escócia e Inglaterra continuou

18
00:00:48,741 --> 00:00:51,647
até o Tratado de União, em 1707.

19
00:00:53,271 --> 00:00:55,515
Então... vamos começar...
com meu molho ao pêssego!

20
00:00:55,627 --> 00:00:58,392
Vou cortar...
Pêssegos, cebolas e panceta!

21
00:00:58,465 --> 00:01:00,502
Com cortes belos e uniformes.

22
00:01:02,179 --> 00:01:04,338
Agora, vocês precisam colocar 
um pouquinho de biri...

23
00:01:04,342 --> 00:01:05,367
<i>*suspiros de surpresa*

24
00:01:05,367 --> 00:01:06,406
whiskey!

25
00:01:06,508 --> 00:01:07,616
Então, vocês aí...

26
00:01:07,694 --> 00:01:08,721
O que a gente usa, agora?

27
00:01:08,715 --> 00:01:09,624
Aspargo!

28
00:01:09,595 --> 00:01:10,461
Aspargo!

29
00:01:10,461 --> 00:01:11,503
Quebrem o aspargo e...

30
00:01:12,299 --> 00:01:14,669
Ok... Agora, a gente precisa da 
panela em temperatura média.

31
00:01:15,772 --> 00:01:17,414
E pronto! 
O que falta fazer, agora?

32
00:01:17,712 --> 00:01:18,741
Empratar!

33
00:01:19,313 --> 00:01:20,317
Beleza, galera! É moleza...

34
00:01:20,317 --> 00:01:21,490
Só precisa arrancar essas folhas...

35
00:01:21,490 --> 00:01:23,321
Coloquem nessa...
hmmm... nessa tigela aqui!

36
00:01:23,873 --> 00:01:24,890
Beleza! Agora: coloco óleo...

37
00:01:24,890 --> 00:01:27,393
coloco sal e espalho...

38
00:01:27,393 --> 00:01:28,872
espalho bem! Por toda a forma...

39
00:01:29,143 --> 00:01:30,434
Coloco no forno e: fim de papo!

40
00:01:30,761 --> 00:01:32,179
Agora, pego o milho...

41
00:01:32,330 --> 00:01:34,695
coloco metade aqui...

42
00:01:34,695 --> 00:01:36,816
e a outra metade bato 
no liquidificador.

43
00:01:40,223 --> 00:01:41,589
Sempre vamos querer 
um purê de milho...

44
00:01:41,589 --> 00:01:42,610
Derramo tudo aqui...

45
00:01:42,772 --> 00:01:44,027
e finalizo com cebolinha.

46
00:01:44,027 --> 00:01:45,109
Pegaram?

47
00:01:45,253 --> 00:01:46,430
Agora, com todo esse resíduo...

48
00:01:46,932 --> 00:01:48,605
vou fazer um molho. Ta certo?

49
00:01:48,724 --> 00:01:49,756
Precisamos colocar um pouco de mel...

50
00:01:49,892 --> 00:01:50,903
um copo de caldo de galinha...

51
00:01:51,691 --> 00:01:53,110
e deixamos reduzir até virar calda.

52
00:01:55,667 --> 00:01:57,483
Vamos ver... Vamos ver...
Cadê vocês, galera?

53
00:01:58,863 --> 00:02:01,541
Deixa eu minimizar minha tela...
Eu não preciso me ver!

54
00:02:01,541 --> 00:02:02,873
Só preciso ver vocês, galera!

55
00:02:03,674 --> 00:02:05,211
Só preciso ver a galera!

56
00:02:07,375 --> 00:02:09,117
Olá, mundo! Aqui é o Raj!

57
00:02:09,352 --> 00:02:11,856
Eu tô maluco pra começar a aula!

58
00:02:12,507 --> 00:02:13,349
Vamos lá?!

59
00:02:13,813 --> 00:02:15,846
A aula tá ao vivo, galera!

60
00:02:16,254 --> 00:02:17,707
Já, já vamos começar 
com alguns cáculos...

61
00:02:17,951 --> 00:02:18,844
nessa live!

62
00:02:18,838 --> 00:02:19,991
Eu tô muito empolgado!

63
00:02:20,186 --> 00:02:22,484
Mas antes de tudo: 
me deixa fazer a chamada.

64
00:02:22,589 --> 00:02:24,288
Beleza? Então, vamos ver...

65
00:02:24,288 --> 00:02:24,804
Collin...

66
00:02:24,804 --> 00:02:25,344
Brandon...

67
00:02:25,344 --> 00:02:25,794
Neil...

68
00:02:25,833 --> 00:02:26,304
David...

69
00:02:26,304 --> 00:02:27,073
Prakash...

70
00:02:27,325 --> 00:02:28,003
Sebastian...

71
00:02:28,036 --> 00:02:28,704
Raj...

72
00:02:28,867 --> 00:02:29,632
Spencer...

73
00:02:29,860 --> 00:02:30,629
Naresh...

74
00:02:31,631 --> 00:02:32,324
Nickel...

75
00:02:32,329 --> 00:02:33,029
Clement...

76
00:02:34,655 --> 00:02:35,539
Vamos lá, pessoal?!

77
00:02:35,645 --> 00:02:36,209
Michael...

78
00:02:36,282 --> 00:02:36,874
Benjamin

79
00:02:37,696 --> 00:02:38,347
Pronto!

80
00:02:38,571 --> 00:02:39,760
Isso que é uma chamada!

81
00:02:40,235 --> 00:02:43,012
Hmm... Sejam bem-vindos à live!

82
00:02:43,051 --> 00:02:44,267
A aula será sobre dee..

83
00:02:44,275 --> 00:02:44,834
sobre dee..

84
00:02:44,935 --> 00:02:47,382
deep learning... 
uma introdução à deep learning.

85
00:02:47,633 --> 00:02:50,422
Vai ser muuuuuito legal!

86
00:02:50,422 --> 00:02:52,701
Legal porque fiquei esperando 
para fazer alguns cálculos...

87
00:02:52,874 --> 00:02:54,028
E adivinhem, só? Adivinhem!

88
00:02:54,123 --> 00:02:57,214
Eu comprei... essa... 
mesa digitalizadora!

89
00:02:57,365 --> 00:02:58,818
Vai servir para fazer os cáuculos.

90
00:02:58,818 --> 00:03:00,743
Beleza! Tô muito 
ansioso para usar ela porque...

91
00:03:00,753 --> 00:03:02,146
nunca usei durante a live!

92
00:03:02,543 --> 00:03:04,963
Hoje, vou mostrar a 
matemática por trás da...

93
00:03:05,007 --> 00:03:06,232
regressão... linear!

94
00:03:06,232 --> 00:03:07,618
Até o fim desse vídeo...

95
00:03:07,897 --> 00:03:09,780
vocês saberão...

96
00:03:10,439 --> 00:03:12,233
o processo para...

97
00:03:12,317 --> 00:03:13,681
fazer regressão radial...

98
00:03:13,776 --> 00:03:15,118
como se não fosse nada!

99
00:03:15,353 --> 00:03:17,409
E isso inclui... 
O método do gradiente!

100
00:03:17,565 --> 00:03:19,347
E já adianto: a gente 
usa o método do gradiente

101
00:03:19,571 --> 00:03:21,234
em tudo que é canto, 
em <i>machine learning.

102
00:03:21,390 --> 00:03:22,930
Mas não se preocupe 
se você não entende disso!

103
00:03:22,935 --> 00:03:24,399
Vou mostrar como 
se faz, ainda assim. Ok?!

104
00:03:24,539 --> 00:03:26,359
Então vamos se 
aprofundar mais nisso!

105
00:03:26,605 --> 00:03:27,150
Então...

106
00:03:27,262 --> 00:03:27,858
Hm...
